var mongoose = require('mongoose');

var commentSchema = mongoose.Schema({
  post_id: String,
  content: String,
  created_at: {type: Number}
});

mongoose.model('comment', commentSchema);
module.exports = mongoose.model('comment');