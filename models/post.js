var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
  title: String,
  content: String,
  created_at: {type: Number}
});

mongoose.model('post', postSchema);
module.exports = mongoose.model('post');