var express = require('express');
var router = express.Router();
var moment = require('moment');

var post = require('../models/post');

router.get('/', function(req, res, _) {
  post.find({}, function (err, posts) {
    if(err) return res.status(500).send('Whoops! Watch out');
    return res.send(posts);
  });
});

router.post('/', function(req, res, _) {
  post.create({
    title: req.body.title,
    content: req.body.content,
    created_at: moment.now()
  }, function(err, post) {
    if(err) return res.status(500).send('Whoops! insert failed');
    return res.send(post);
  });
});

router.put('/:id', function(req, res, _) {
  post.findByIdAndUpdate({
    _id: req.params.id
  }, {
    title: req.body.title,
    content: req.body.content
  }, function(err, post) {
    if(err) return res.status(500).send('Whoops! update failed');
    return res.send(post);
  });
});

router.delete('/:id', function(req, res, _) {
  post.findOneAndRemove({
    _id: req.params.id
  }, function (err, post) {
    if(err) return res.status(500).send('Whoops! delete failed');
    return res.status(204).send(post);
  });
});

module.exports = router;
