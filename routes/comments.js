var express = require('express');
var router = express.Router();
var moment = require('moment');

var post = require('../models/post');
var comment = require('../models/comment');

router.get('/', function(req, res, _) {
  comment.find({
    post_id: req.query.post_id
  }, function (err, posts) {
    if(err) return res.status(500).send('Whoops! Watch out');
    return res.send(posts);
  });
});

router.post('/', function(req, res, _) {
  // is_post_exists = post.count({
  //   _id: req.params.post_id
  // });
  // if(!is_post_exists)
  //   if(err) return res.status(500).send('related post not found');
  comment.create({
    post_id: req.query.post_id,
    content: req.body.content,
    created_at: moment.now()
  }, function(err, comment) {
    if(err) return res.status(500).send('Whoops! insert failed');
    return res.send(comment);
  });
});

router.put('/:id', function(req, res, _) {
  comment.findByIdAndUpdate({
    _id: req.params.id
  }, {
    content: req.body.content
  }, function(err, comment) {
    if(err) return res.status(500).send('Whoops! update failed');
    return res.send(comment);
  });
});

router.delete('/:id', function(req, res, _) {
  comment.findOneAndRemove({
    _id: req.params.id
  }, function (err, comment) {
    if(err) return res.status(500).send('Whoops! delete failed');
    return res.status(204).send(comment);
  });
});

module.exports = router;
